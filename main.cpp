#include <iostream>
#include <math.h>
#include <string.h>

using namespace std;

class MyString
{

    private :
        char* tab; /// Adresse contenant des tableaux de caract�re (PRINCIPAL)
        int n; /// Remplace le \0 = d�limiteur de fin de chaine (SECONDAIRE)
        int spe; /// compteur de caract�re sp�ciaux grossier  qui n'est pas dans [A-Z] (SECONDAIRE)
        int* stat; /// compteur de caract�re dans [A-Z] -> adresse sur un tableau de 26 cases (alphabet) (SECONDAIRE)

        void maj(); /// �a doit pas �tre dispo � l'utilisateur, usage interne, appel� d'une autre m�thode (code du TD3 demander a yanis)

    public :
        MyString(); /// Constructeur � 0
        MyString(char*); /// Constructeur avec tableau de chaine param�tre
        MyString(int,char); /// Constructeur avec nombre du caract�re choisit
        MyString(const MyString&); /// Constructeur par recopie
        ~MyString();

        void affiche();
        void supp(char); /// TD3
        void dedouble(char); /// TD3
        void concatenation(MyString); /// TD3
        void mintomaj();
};

MyString::MyString()
{
    tab = NULL;
    n = spe = 0;
    stat = new int[26];

    for (int i=0; i<26; i++)
        stat[i] = 0;
}

MyString::MyString(char* pch)
{
    n = strlen(pch); /// strlen
    tab = new char[n+1];
    strcpy(tab,pch);/// copier, strcpy
    stat = new int[26];

    maj(); /// initialisation � 0 ici
}

MyString::MyString(int a, char x)
{
    stat = new int[26];
    tab = new char[a+1];
    int i;

    for (i=0; i<a; i++)
        tab[i] = x;
    tab[i] ='\0';

    maj();
}

MyString::MyString(const MyString& s)
{
    n = s.n;
    spe = s.spe;
    tab = new char[n+1];
    strcpy(tab, s.tab);
    stat = new int[26];
    for(int i=0; i<26;i++)
    {
        stat[i]=s.stat[i];

    }

    maj();
}

MyString::~MyString()
{
}

void MyString::affiche()
{
    cout << "Affichage des caracteres"<< endl;
    if (tab!=NULL)
        for (int i=0; i<n; i++)
        {
            cout << tab[i];
        }

    cout << endl << endl<< "Nombre de chaque caracteres"<< endl;
    for (int i=0; i<26; i++)
    {
        cout << "le " << i+1 << "eme caractere apparait " << stat[i] << " fois" <<endl;
    }
}

void MyString::supp(char x)
{
    for (int i=0; tab[i] != '\0';i++)
    {
        if (tab[i] == x)
        {
            for (int p=i; tab[p] != 0;p++)
            {
                tab[p] = tab[p+1];
            }
            i--;
        }
    }
}

void MyString::dedouble(char x)
{

}

void MyString::concatenation(MyString s)
{
    char* result; ///concatenation des 2 tableaux
    /// delete le tab a la fin
    /// tab = 1er tableau
    /// s.tab = 2eme tableau de la concatenation

    int i=0,j;
    result = new char[n+s.n+1]; /// taille des 2 tableaux

    while (tab[i]!= '\0')
    {
        result[i] = tab[i];
        i++;
    }
    j = i;
    i = 0;
    while (s.tab[i]!= '\0')
    {
        result[j] = s.tab[i];
        i++;
        j++;
    }
    result[j]='\0';
    delete tab;
    tab = result;

    maj();
}

void MyString::maj()
{
    int i;
    spe = 0;

    for (i=0; i<26; i++) /// init � 0, cases de stat[i]
        stat[i] = 0;

    for (i=0; tab[i]!='\0'; i++) /// parcours de chaine a faire (for(i;tab[i]!=\0)) ----- c = toupper(tab[i]); si c appartient � [A-Z], alors incr�mentation de stat[?]++ (c-'A'), sinon on incr�mente spe; Fin de parcours
    {
        char c = toupper(tab[i]);

        if ((c<='Z') && (c>='A'))
            stat[c - 'A']++;
        else
            spe++;
    }
    n = i; /// n=i
}

void MyString::mintomaj()
{

}

int main ()
{
    MyString s1; s1.affiche();

    MyString s2("abcba"); s2.affiche();

    MyString s3(4,'b'); s3.affiche(); /// Affiche "bbbb" et "il y a 4 b"
   // MyString s4(s3); s4.affiche(); /// = s3
}
